import setuptools

with open("README.md", "r") as f:
    long_description = f.read()

setuptools.setup(
    name="corpora",
    author="Grammatech",
    description="Scripts for the ingestion, management, and analysis of python corpora for machine learning.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/corpora",
    packages=[
        "corpora.callsites_zodb",
        "corpora.common",
        "corpora.ingestion",
        "corpora.simplified_asts_zodb",
    ],
    package_dir={"": "src"},
    python_requires=">=3.6",
    classifiers=[
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    entry_points={
        "console_scripts": [
            "callsites-zodb = corpora.callsites_zodb.callsites_zodb:main",
            "callsites-zodb-postprocess = corpora.callsites_zodb.callsites_zodb_postprocess:main",
            "simplified-asts-zodb = corpora.simplified_asts_zodb.simplified_asts_zodb:main",
            "exempla-gratis-zodb = corpora.simplified_asts_zodb.exempla_gratis_zodb:main",
        ]
    },
    install_requires=[
        "asts",
        "compress_pickle",
        "dvc",
        "jedi",
        "pathos",
        "tqdm",
        "zodb",
        "RelStorage[sqlite3]",
        "kitchensink @ git+https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/kitchensink@master",
    ],
    extras_require={"dev": ["black", "flake8", "pre-commit", "pytest"]},
)
