# Description

Scripts for the ingestion and analysis of python corpora for
downstream machine learning tasks.

# Requirements

The corpora repository requires python 3.6 or higher.

# Installation

## Developer install

If you are working on the development of this repository or
are a client looking for more frequent updates, you may wish
to install the package using the instructions given below.

The dependencies for this repository may be installed by executing
`pip3 install -r requirements.txt`.  If you are contributing you
will want to install the pre-commit hooks found in the
[pre-commit config](./pre-commit-config.yaml) by executing
`pre-commit install`.

After installing dependencies, you must place the repository's
src/ directory on your $PYTHONPATH.  From the base of this
repository, this may be accomplished with the following command:

```
export PYTHONPATH=$PWD/src:$PYTHONPATH
```

Additionally, you will need to clone the
[kitchensink](https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/kitchensink)
repository, install its requirements, and place its src/ directory on
your $PYTHONPATH.  The kitchensink repository is a dependency of this
repository under current development.  This setup may be done with the
following sequence of commands, to be run from the parent directory
of this repository:

```
git clone git@gitlab.com:GrammaTech/Mnemosyne/learn-synth/kitchensink.git
cd kitchensink
pip3 install -r requirements.txt
export PYTHONPATH=$PWD/src:$PYTHONPATH
```

## Client Install

If you are using this library solely as a client, you may
create a python wheel file from this repository and install
it by executing the following sequence of commands:

```
python3 setup.py bdist_wheel --universal
pip3 install dist/*
```

This will also create and add `callsites-zodb`, `callsites-zodb-postprocess`,
`simplified-asts-zodb`, and `exempla-gratis-zodb` commands to your $PATH
which may be invoked directly instead of the corresponding `callsites_zodb.py`,
`callsites_zodb_postprocess.py`, `simplified_asts_zodb.py` and
`exempla_gratis_zodb.py` scripts in the instructions below.

# Data Version Control

This repository utilizes [Data Version Control](https://dvc.org/)
to manage machine learning datasets.  The `dvc` command is installed
after running the instructions listed in the installation section.
The `data/` directory contains subdirectories for language-specific
corpora along with `*.dvc` files with information allowing dvc
to track data files.  Raw datasets are stored in `raw` subdirectories
while ingested/deduplicated datasets are stored in `ingested`
subdirectories.  Remote data storage exists in the Amazon S3
instance for IDAS under the `python-dataset/dvc` bucket.
Additional documentation on DVC may be found at https://dvc.org/doc.

If you have not yet setup your AWS credentials on the machine
you are working on, you will need to use the `dvc remote modify`
command to set both the `access_key_id` and `secret_access_key` for
your Amazon account.  To find your access key, after signing into the
AWS Management console, in the navigation bar on the upper right,
choose your account name or number and then choose
`My Security Credentials`.  Under the `Access keys for CLI, SDK,
& API access` section, you will see the access key ID.
The secret key was created when the access key was generated;
unfortunately, there is no way to get the secret key if it was
not written down during setup.  If you do not have access to the
secret key, you may create a new access key (writing down the new
secret key after setup) by clicking the `Create access key` button.
If you do not have permissions to create a new access key, contact
IT and they can set one up for you.

After locating the access key and secret key, run the following
commands to complete credential setup:

```
dvc remote modify --local aws access_key_id <access_key_id>
dvc remote modify --local aws secret_access_key <secret_access_key>
```

If after setting up your credentials you experience authentication issues,
https://dvc.org/doc/user-guide/troubleshooting#no-credentials
may prove useful.

Once the credentials have been setup, you may use `dvc pull`
to bring all datasets into the data directory.  However, because
some datasets (e.g. karampatsis2020) are very large, it is
recommended to pull down single datasets as needed;
you may use `dvc pull <target> <targets ...>` to bring in a
single dataset or subset of datasets.  For example you could
run `dvc pull data/python/raw/typewriter` to bring in the
typewriter dataset.  The original, unprocessed datasets are stored
in `raw` subdirectories while the ingested/deduplicated datasets
are stored in `ingested` subdirectories.

# Callsite ZODB Database

A ZODB database containing frequency counts on the usage of identifiers
and constants at function callsites can be created or updated
using the `callsites_zodb.py` script in `src/corpora/callsites_zodb`.
You may view the help for this script by executing the following command:

```
python3 callsites_zodb.py --help
```

The script takes an ingested corpus as input; ingested
corpura may be found in the `data/ingested` directory of this
repo.  The ZODB instance created or modified is output to
`callsites.db` by default; this may be changed via the
`--database` argument to the script.

An example usage is given below:

```
python3 callsites_zodb.py /path/to/ingested/corpus
```

Once created and populated from the appropriate datasets, the ZODB
instance can be post-processed to generate scores for use
downstream in the
[argument prediction muse](https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/code-predictor).
The `callsites_zodb_postprocess.py` script in `src/corpora/callsites_zodb`
performs this postprocessing.  You may view the help for this script
by executing the following command:

```
python3 callsites_zodb_postprocess.py --help
```

The script takes a ZODB instance created above as input; additionally,
several configuration parameters exist to control identifier and
constant scoring.  An example usage is given below:

```
python3 callsites_zodb_postprocess.py callsites.db
```

# Simplified AST ZODB Database

A ZODB database containing simplified ASTs for use in example generation
can be created or updated using the `simplified_asts_zodb.py` script in
`src/corpora/simplified_asts_zodb.py`.  You may view the help for this
script by executing the following command:

```
python3 simplified_asts_zodb.py --help
```

The script takes an ingested corpus as input; ingested corpora may be
found in the `data/ingested` directory of this repo.  The ZODB
instance created or modified is output to `simplifed_asts.db` by
default; this may be changed via the `--database` argument to the
script.

An example usage is given below:

```
python3 simplified_asts_zodb.py /path/to/ingested/corpus
```

Once created and populated from the appropriate datasets, the ZODB
instance can be post-processed to generate API usage examples using
the algorithm described in "Exempla Gratis (E.G.): Code Examples for
Free".  These API usage examples can be utilized downstream in
the [snippet mining muse](https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/snippet-mining).
The `exempla_gratis_zodb.py` script performs this postprocessing.
You may view the help for this script by executing the following
command:

```
python3 exempla_gratis_zodb.py --help
```

The script takes a ZODB instance of simplified ASTs created above
as input; additionally, several configuration parameters exist
to control the example generation algorithm.  Moroever, the
`--num-threads` argument to the script may be used to adjust
the number of threads used in processing.  An example usage
is given below:

```
python3 exempla_gratis_zodb.py simplified_asts.db/
```
