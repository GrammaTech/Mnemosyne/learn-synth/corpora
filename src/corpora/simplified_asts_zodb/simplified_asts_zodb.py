"""
Script to create a ZODB of simplified ASTs.
"""

import hashlib
import pathlib
import sys

import ZODB

import kitchensink.utility.filesystem as fs

from typing import List, Dict, NamedTuple, Optional
from compress_pickle import dumps
from persistent.mapping import PersistentMapping
from persistent.list import PersistentList
from kitchensink.utility.simplified_asts import SimplifiedAST
from corpora.common.zodb import common_main
from corpora.simplified_asts_zodb.common import HASHES_KEY, TREES_KEY, REFS_KEY, get
from asts import (
    AST,
    ASTLanguage,
    ASTException,
    ClassAST,
    FunctionAST,
    LambdaAST,
    IdentifierAST,
)

DEFAULT_DATABASE_DIR = pathlib.Path("simplified_asts.db")


def process(
    path: pathlib.Path,
    database: ZODB.DB,
    corpus: pathlib.Path,
    *args: List,
    **kwargs: Dict,
) -> None:
    """
    Populate database with simplified ASTs created from the functions
    in @path.  Additionally, for each function callsite in script,
    populate a reference link to the simplified AST it is contained
    within.

    :param path: source text filepath
    :param database: ZODB instance to populate
    :param corpus: path to the corpus containing the file we are processing
    """

    class SimplifiedASTTreeDBEntry(NamedTuple):
        pathname: str
        function_name: str
        pickled_tree: bytes

    class SimplifiedASTRefDBEntry(NamedTuple):
        language: str
        pathname: str
        function_name: str
        callsite_module: str
        callsite_function: str

    def is_unique_hash(root: PersistentMapping, hcode: int) -> bool:
        """
        Return true if the hashcode has not yet appeared.

        :param root: root of the ZODB instance containing existing hashes
        :param hcode: hashcode to test
        """
        data = get(root, HASHES_KEY, PersistentList())
        return hcode not in data

    def is_valid_simplified_tree(simplified_ast: SimplifiedAST, function: AST) -> bool:
        """
        Return true if @simplified_ast is valid.

        :param simplified_ast: simplified AST tree
        :param function: function AST from which the simplified AST was derived
        """
        try:
            text = simplified_ast.text(prettify=False) if simplified_ast else ""
            return text.strip() != ""
        except TypeError:
            # occurs with non-UTF8 character encodings and positional mismatches
            # leading to simplified ASTs which are not well formed
            return False

    def not_in_nested_function(root: AST, function: AST, node: AST) -> bool:
        """
        Return true if @node is nested in a function AST other than @function

        :param root: root of the AST containing @function and @node
        :param function: function AST containing node and potentially other
                         nested functions
        :param node: generic AST node
        """
        while node:
            if isinstance(node, FunctionAST) and not isinstance(node, LambdaAST):
                return node == function
            node = node.parent(root)
        return False

    def qualified_name(root: AST, function: AST) -> str:
        """
        Return the fully qualified name of the function, including enclosing classes
        for methods and enclosing functions for inner functions.

        :param root: root of the AST containing @function
        :param function: function AST
        :return: fully qualified name of @function
        """
        parts = []
        node = function
        while node:
            if isinstance(node, FunctionAST):
                parts.append(node.function_name())
            elif isinstance(node, ClassAST):
                identifier = first_identifier_child(node)
                identifier = identifier.source_text if identifier else ""
                parts.append(identifier)
            node = node.parent(root)
        parts.reverse()

        return ".".join(parts)

    def first_identifier_child(node: AST) -> Optional[AST]:
        """
        Return the first child of @node which is an identifier AST, if possible.

        :param node: generic AST node
        :return: first identifier child of @node
        """
        return next((c for c in node.children if isinstance(c, IdentifierAST)), None)

    def callsite_function_name(callsite: AST) -> Optional[str]:
        """
        Return the name of the function at @callsite, if possible.

        :param calliste: function callsite AST node
        :return: name of the function, if possible
        """
        node = callsite.call_function()

        if isinstance(node, IdentifierAST):
            return node.source_text

        if isinstance(node.children[-1], IdentifierAST):
            return node.children[-1].source_text

        return None

    def md5_hash(function: AST) -> int:
        """
        Return a deterministic hashcode for the given function using the
        md5 hashing algorithm.

        :param node: function AST
        """
        code = function.source_text.strip().encode()
        digest = hashlib.md5(code).digest()
        hcode = int.from_bytes(digest, byteorder=sys.byteorder, signed=False)
        return hcode % 2**64

    def remove_unreferenced_trees(
        trees: List[SimplifiedASTTreeDBEntry],
        refs: List[SimplifiedASTRefDBEntry],
    ) -> List[SimplifiedASTTreeDBEntry]:
        """
        Remove entries from @trees which do not have a corresponding entry
        in @refs.

        :param trees: list of simplified ASTs to insert into the database
        :param refs: list of API usage references to insert into the database
        """
        results = []

        for tree in trees:
            for ref in refs:
                if (
                    ref.pathname == tree.pathname
                    and ref.function_name == tree.function_name
                ):
                    results.append(tree)

        return results

    def get_hashes(database: ZODB.DB) -> List[int]:
        """
        Return a list of hashes in the ZODB database.

        :param database: ZODB database
        """
        with database.transaction() as conn:
            return get(conn.root(), HASHES_KEY, PersistentList()).data.copy()

    def add_hashes(root: PersistentMapping, hcodes: List[int]) -> bool:
        """
        Add hashcodes to the database at @root.

        :param root: root node of the ZODB instance
        :param hcodes: list of hashcodes
        """
        data = get(root, HASHES_KEY, PersistentList())
        new = list(set(hcodes) - (set(data.data)))
        data.extend(new)

    def add_simplified_tree(
        root: PersistentMapping,
        entry: SimplifiedASTTreeDBEntry,
    ) -> None:
        """
        Add a simplified tree entry to the database at @root.  We are
        building a mapping of pathnames -> functions -> trees.

        :param root: root node of the ZODB instance
        :param entry: database entry for a simplified AST tree
        """
        data = get(root, TREES_KEY, PersistentMapping())
        data = get(data, entry.pathname, PersistentMapping())
        if function_name not in data:
            data[entry.function_name] = entry.pickled_tree

    def add_callsite_ref(
        root: PersistentMapping,
        entry: SimplifiedASTRefDBEntry,
    ) -> None:
        """
        Add an API usage reference to the database at @root.  We are
        building a mapping of api usages -> simplified ASTs.
        Later, when generating examples of API usage, this mapping will
        allow us to get all of the simplified ASTs containing a call to
        the given API.

        :param root: root node of the ZODB instance
        :param entry: database entry for an API usage reference
        """
        data = get(root, REFS_KEY, PersistentMapping())
        data = get(data, entry.language, PersistentMapping())
        data = get(data, entry.callsite_module, PersistentMapping())
        data = get(data, entry.callsite_function, PersistentMapping())
        data = get(data, entry.pathname, PersistentList())
        if entry.function_name not in data:
            data.append(entry.function_name)

    language = ASTLanguage.Python
    pathname = str(path.relative_to(corpus))

    # Parse the file at path and retrieve a list of its functions.
    try:
        script = AST.from_string(fs.safe_slurp(path), language=language)
        functions = script.function_asts()
    except ASTException:  # encoding errors, skip
        functions = []

    # Iterate over each function and create simplified AST trees.
    hashes = get_hashes(database)
    trees = []
    refs = []
    for function in functions:
        hcode = md5_hash(function)

        if hcode not in hashes and not isinstance(function, LambdaAST):
            # If the function is unique and not a lambda add it to the
            # list of trees to be stored in the database.
            function_name = qualified_name(script, function)
            simplified_tree = SimplifiedAST.from_sel(script, function)
            if not is_valid_simplified_tree(simplified_tree, function):
                continue

            pickled_tree = dumps(simplified_tree.to_json(), compression="gzip")
            hashes.append(hcode)
            trees.append(
                SimplifiedASTTreeDBEntry(
                    pathname=pathname,
                    function_name=function_name,
                    pickled_tree=pickled_tree,
                )
            )

            # Iterate over the function callsites in the function.  If
            # the module and function name can be derived from the callsite
            # and the callsite is not in a nested function, add it to the list
            # of API references contained within the tree.
            body = function.function_body()
            function_callsites = body.call_asts()
            for callsite in function_callsites:
                callsite_module = callsite.provided_by(script)
                callsite_function = callsite_function_name(callsite)

                if (
                    callsite_module
                    and callsite_function
                    and not_in_nested_function(script, function, callsite)
                ):
                    refs.append(
                        SimplifiedASTRefDBEntry(
                            language=language.name.lower(),
                            pathname=pathname,
                            function_name=function_name,
                            callsite_module=callsite_module,
                            callsite_function=callsite_function,
                        )
                    )

    # Remove tree entries which are not referenced in refs.
    trees = remove_unreferenced_trees(trees, refs)

    # Store the results in the database.
    with database.transaction() as conn:
        root = conn.root()

        # Store the hashes of the newly added trees to avoid duplicates.
        add_hashes(root, hashes)

        # Store the simplified AST trees, building a mapping of
        # file -> function -> simplified tree.
        for tree in trees:
            add_simplified_tree(root, tree)

        # For each function callsite, store a reference to the simplified
        # AST it is contained within, building a mapping of api usage ->
        # file -> functions.  Later, when generating examples of API
        # usage, this mapping will allow us to get all of the simplified
        # ASTs containing a call to the given API.
        for ref in refs:
            add_callsite_ref(root, ref)


def main():
    common_main(
        process,
        docstring=__doc__,
        default_database_dir=DEFAULT_DATABASE_DIR,
    )


if __name__ == "__main__":
    main()
