"""
Create a database of API usage examples from a simplified AST database.
"""

from argparse import ArgumentParser
from collections import Counter
from copy import deepcopy
from dataclasses import dataclass
from logging import basicConfig, info, INFO
from pathlib import Path
from random import Random
from typing import Dict, Generator, List, Set, Tuple
from compress_pickle import dumps, loads
from pathos.multiprocessing import cpu_count, Pool
from persistent.mapping import PersistentMapping
from persistent.list import PersistentList
from tqdm import tqdm
from ZODB import DB
from kitchensink.utility.simplified_asts import ASTPath, SimplifiedAST
from kitchensink.utility.zodb import DatabasePool
from corpora.simplified_asts_zodb.common import TREES_KEY, REFS_KEY, get

API_Type = Tuple[str, str, str]

DEFAULT_EXEMPLA_GRATIS_DATABASE_DIR = Path("exempla_gratis.db")
DEFAULT_NUM_THREADS = max(cpu_count() // 2, 1)
DEFAULT_MAX_EXAMPLES = 20
DEFAULT_MIN_SAMPLES = 100
DEFAULT_MAX_SAMPLES = 25000
DEFAULT_MIN_ITERATIONS = 5
DEFAULT_MAX_ITERATIONS = 100
DEFAULT_SPECIFICITY_THRESHOLD = 0.05
DEFAULT_NEW_EDGE_THRESHOLD = 0.35
DEFAULT_MAX_FILLER_NODES = 15
DEFAULT_MAX_FILLER_CHARACTERS = 50
DEFAULT_SEED = 0


@dataclass(eq=True, frozen=True)
class ExemplaGratisConfig:
    """
    configuration POD with fields controlling the E.G. algorithm
    """

    max_examples: int = DEFAULT_MAX_EXAMPLES
    seed: int = DEFAULT_SEED
    min_samples: int = DEFAULT_MIN_SAMPLES
    max_samples: int = DEFAULT_MAX_SAMPLES
    min_iterations: int = DEFAULT_MIN_ITERATIONS
    max_iterations: int = DEFAULT_MAX_ITERATIONS
    specificity_threshold: float = DEFAULT_SPECIFICITY_THRESHOLD
    new_edge_threshold: float = DEFAULT_NEW_EDGE_THRESHOLD
    max_filler_nodes: int = DEFAULT_MAX_FILLER_NODES
    max_filler_characters: int = DEFAULT_MAX_FILLER_CHARACTERS


def iter_apis(database: DB) -> Generator[API_Type, None, None]:
    """
    yield the distinct APIs represented in @database as
    (language, module, function) tuples

    :param database: database of simplified ASTs
    """
    with database.transaction() as conn:
        root = conn.root()
        refs = root.get(REFS_KEY, {})

        for language in refs.keys():
            for module in refs[language].keys():
                for function in refs[language][module].keys():
                    yield (language, module, function)


def query_trees(
    database: DB,
    language: str,
    module: str,
    function: str,
) -> List[SimplifiedAST]:
    """
    query for the simplified ASTs in @database containing usages
    of the API described by @language, @module, and @function

    :param database: database of simplified ASTs
    :param language: source code language of the API function
    :param module: module containing the API function
    :param function: name of the API function
    """
    results = []

    with database.transaction() as conn:
        root = conn.root()
        refs = root.get(REFS_KEY, {})

        for path in refs[language][module][function].keys():
            for name in refs[language][module][function][path]:
                if path in root[TREES_KEY] and name in root[TREES_KEY][path]:
                    bits = root[TREES_KEY][path][name]
                    results.append(bits)

    results = [loads(tree, compression="gzip") for tree in results]
    results = [SimplifiedAST.from_json(tree) for tree in results]

    return results


def exempla_gratis(
    language: str,
    module: str,
    function: str,
    trees: List[SimplifiedAST],
    config: ExemplaGratisConfig = ExemplaGratisConfig(),
) -> List[SimplifiedAST]:
    """
    return common patterns from example usages of @function in @trees
    using the algorithm described in "Exempla Gratis (E.G.): Code Examples
    for Free"

    :param trees: list of simplified AST trees containing a usage of @function
    :param function: name of the function we are looking for usage patterns of
    :param config: configuration constants for the algorithm
    """

    def min_pattern(language: str, module: str, function: str) -> int:
        """
        Return the mininum number of nodes which will be in the E.G. pattern.
        """
        if language != "python" or module == "builtins":
            return 1
        else:
            return 1 + 2 * len(module.split("."))

    def initial_matches(
        token: str,
        trees: List[SimplifiedAST],
    ) -> List[SimplifiedAST]:
        """
        return a list of subtrees of @trees matching @token

        :param token: string to match
        :param trees: list of simplified ASTs
        """
        subtrees = []

        for tree in trees:
            for subtree in tree.traverse():
                if token == subtree.token_or_label():
                    subtree.mark()
                    subtrees.append(subtree)
                    break

        return subtrees

    def extend(
        subtrees: List[SimplifiedAST],
        used_nodes: Set[SimplifiedAST],
        config: ExemplaGratisConfig = config,
    ) -> Tuple[List[SimplifiedAST], List[SimplifiedAST]]:
        """
        extend the example pattern currently found in the marked
        nodes of @subtrees by adding a new node to the pattern,
        returning the subtrees with the new pattern and the new
        nodes added to the pattern

        :param subtrees: list of trees containing a common example usage
            pattern
        :param used_nodes: set of nodes already used in a pattern or patterns
        :param config: configuration constants for the algorithm
        """

        def unmarked_edge_nodes(
            subtree: SimplifiedAST,
        ) -> List[Tuple[ASTPath, SimplifiedAST]]:
            """
            for the given subtree, return the edges (both path and node)
            which are not currently marked as part of the pattern

            :param subtree: subtree containing an example usage pattern
            """

            def helper(node: SimplifiedAST) -> List[Tuple[ASTPath, SimplifiedAST]]:
                """
                recursive helper function to find edges not currently
                in the pattern

                :param node: node in the subtree
                """
                results = []
                if not node.marked:
                    results.append((subtree.relative_path(node), node))
                else:
                    for child in node.children:
                        results.extend(helper(child))
                return results

            results = []
            if subtree.parent:
                results.append((subtree.relative_path(subtree.parent), subtree.parent))
            results.extend(helper(subtree))

            return results

        def new_edge(
            subtrees: List[SimplifiedAST],
            used_nodes: Set[SimplifiedAST],
            config: ExemplaGratisConfig = config,
        ) -> List[SimplifiedAST]:
            """
            return a list of nodes in a subset of @subtrees representing a new
            edge to incorporate into the example pattern

            :param subtrees: list of subtrees containing an example usage pattern
            :param used_nodes: set of nodes already used in a pattern or patterns
            :param config: configuration constants for the algorithm
            """
            edges = {}
            r = Random(config.seed)

            # Build a mapping of unmarked edges to the tokens/labels
            # present at that edge and the associated node(s) with
            # those tokens or labels.
            for subtree in subtrees:
                for path, node in unmarked_edge_nodes(subtree):
                    key = node.token_or_label()
                    if path not in edges:
                        edges[path] = {}
                    if key not in edges[path]:
                        edges[path][key] = []
                    edges[path][key].append(node)

            # Shuffle the paths to each potential new edge.  This
            # will ensure in the case of a tie sorting in the next
            # step, the path choosen is random.
            paths = list(edges.keys())
            r.shuffle(paths)

            # Sort the edges such that the edges with the greatest number
            # nodes sharing a common pattern/label appear first.
            edges = [nodes for path in paths for nodes in edges[path].values()]
            edges = sorted(edges, key=len, reverse=True)

            # If possible, find an edge which has not been utilized in
            # previous patterns but where the number of nodes in the
            # edge divided by the number of nodes in the "best" edge
            # exceeds a threshold.  If so, choose this new edge to
            # incorporate into the pattern; this allows us to create
            # multiple new usage examples.
            for edge in edges:
                if not used_nodes.intersection(set(edge)):
                    if len(edge) / len(edges[0]) > config.new_edge_threshold:
                        return edge
                    else:
                        break

            # Fallback to returning the best edge.
            return edges[0] if edges else []

        # Find the nodes in the new edge to be added.
        return new_edge(subtrees, used_nodes, config)

    def new_subtrees(
        new_edge_nodes: List[SimplifiedAST],
    ) -> List[SimplifiedAST]:
        """
        mark the new edge nodes as belonging to the example pattern
        and return a list of new subtrees containing the new edge nodes

        :param new_edge_nodes: list of nodes for a new edge to incorporate
            into the example pattern
        """
        results = []

        for node in new_edge_nodes:
            node.mark()
            while node.parent and node.parent.marked:
                node = node.parent
            results.append(node)

        return results

    def best(
        subtrees: List[SimplifiedAST],
        config: ExemplaGratisConfig = config,
    ) -> SimplifiedAST:
        """
        return the most representative subtree in @subtrees

        :param subtrees: list of subtrees containing an example usage
        :param config: configuration constants for the algorithm
        """

        def is_valid_hole_filler(
            tree: SimplifiedAST,
            config: ExemplaGratisConfig = config,
        ) -> bool:
            """
            predicate for ascertaining if @tree should be used as a
            potential hole filler

            :param tree: potential hole filler tree
            :param config: configuration constants for the algorithm
            """

            # This is a valid hole filler if the number of nodes
            # in the tree is less than or equal to the
            # max_filler_nodes constant from the config and the
            # number of characters in the tree is less than or
            # equal to the max_filler_characters constant from
            # the config.
            return (
                tree.size <= config.max_filler_nodes
                and len(tree.text()) <= config.max_filler_characters
            )

        def compute_hole_filler_counts(
            subtrees: List[SimplifiedAST],
            config: ExemplaGratisConfig = config,
        ) -> Dict:
            """
            return a dictionary mapping paths to each hole in the @subtrees
            examples to a frequency counter of potential filler text

            :param subtrees: list of subtrees containing a common
                usage pattern of a function
            :param config: configuration constants for the algorithm
            """
            result = {}

            for subtree in subtrees:
                for child in subtree.traverse():
                    if not child.marked and child.parent.marked:
                        rel_path = subtree.relative_path(child)

                        if rel_path not in result:
                            result[rel_path] = Counter()
                        if is_valid_hole_filler(child, config):
                            result[rel_path][child.text()] += 1

            return result

        def score(subtree: SimplifiedAST, hole_filler_counts: Dict) -> int:
            """
            score the given @subtree based the frequency its unmarked
            subtrees (holes) appear in @hole_filler_counts

            :param subtree: example subtrees
            :param hole_filler_counts: dictionary mapping paths to holes
                in the example subtree to frequency counts of common
                filler text for the hole
            """
            score = 0

            for path in hole_filler_counts:
                node = subtree.get(path)
                score += hole_filler_counts[path][node.text()]

            return score

        def best_tree(scores: List[Tuple[SimplifiedAST, int]]) -> SimplifiedAST:
            """
            given the list of trees and their scores, return the most
            representative tree for this example

            :param scores: list of tree/score pairs
            """
            best_trees = []
            best_score = -1

            # Create a list of trees with the highest score
            for tree, score in scores:
                if score > best_score:
                    best_trees = []
                    best_score = score
                if score == best_score:
                    best_trees.append(tree)

            # From this list of trees with the same score, find
            # the smallest example and return it.
            return min(best_trees, key=lambda tree: len(tree.text()))

        # First, ascertain counts of potential fillers for each hole in
        # the subtrees.  Score the subtrees based on how often common
        # hole fillers occur.  Once scored, find the subtree with the
        # maximual score and return it.
        counts = compute_hole_filler_counts(subtrees, config)
        scores = [(subtree, score(subtree, counts)) for subtree in subtrees]
        return best_tree(scores)

    def contains_pattern(tree: SimplifiedAST, trees: Set[SimplifiedAST]) -> bool:
        """
        return true if the pattern defined by the marked nodes in @tree can be
        found in @trees, either exactly or as a subpattern

        :param tree: simplified AST with marked nodes delineating a pattern
        :param trees: existing simplified ASTs with marked nodes delineating a pattern
        """

        def pattern(tree: SimplifiedAST) -> Set[Tuple[ASTPath, str]]:
            """
            return a set of (path, token_or_label) pairs for each marked node
            in tree, defining a pattern for the E.G algorithm

            :param tree: simplified AST with marked nodes delineating a pattern
            """
            result = set()

            for node in tree.traverse():
                if node.marked:
                    token_or_label = node.token_or_label() if not node.leaf else None
                    result.add((tree.relative_path(node), token_or_label))

            return result

        # Iterate over each comparison tree in trees and see if the pattern
        # can be found.
        tree_pattern = pattern(tree)
        for comparison in trees:
            if tree_pattern.intersection(pattern(comparison)) == tree_pattern:
                return True
        return False

    def non_unique_branches(tree: SimplifiedAST, trees: Set[SimplifiedAST]) -> bool:
        """
        return true if the branch nodes in @tree can be found in @trees.  Note that
        the branches can be marked or unmarked as part of a pattern; we are only
        testing here if the branch nodes have the same label and sizes.

        :param tree: simplified AST
        :param trees: list of simplified AST trees to search against
        """

        def branches(tree: SimplifiedAST) -> Set[Tuple[ASTPath, str, int]]:
            result = set()

            for node in tree.traverse():
                if not node.leaf:
                    token_or_label = node.token_or_label()
                    result.add((tree.relative_path(node), token_or_label, node.size))

            return result

        tree_branches = branches(tree)
        for comparison in trees:
            if tree_branches == branches(comparison):
                return True
        return False

    # Global data structures for the E.G. algorithm
    used_nodes = set()
    used_subtrees = set()
    min_iterations = config.min_iterations + min_pattern(language, module, function)
    results = []

    # Limit search to max_samples randomly sampled trees.
    trees = Random(config.seed).sample(trees, min(config.max_samples, len(trees)))

    # Find the initial subtrees matching the function we wish to
    # find examples for.
    initial_subtrees = initial_matches(function, trees)

    if len(initial_subtrees) >= config.min_samples:
        # We have trees to query for examples.  Run the E.G. algorithm
        # @max_examples times to generate up to @max_examples examples.
        for i in range(0, config.max_examples):
            # Step #1 - Extend the initial subtrees with new nodes
            # until the percentage of subtrees remaining compared
            # to the number of initial subtrees falls below
            # the specificity threshold.
            subtrees = initial_subtrees
            for j in range(0, config.max_iterations):
                new_edge_nodes = extend(subtrees, used_nodes, config)
                if len(new_edge_nodes) / len(trees) < config.specificity_threshold:
                    break

                used_nodes.update(new_edge_nodes)
                subtrees = new_subtrees(new_edge_nodes)

            # Step #2 - Of the remaining subtrees, find the most
            # representive example.
            best_subtree = best(subtrees, config)
            if (
                contains_pattern(best_subtree, results)
                or non_unique_branches(best_subtree, results)
                or j < min_iterations
            ):
                break
            else:
                used_subtrees.add(best_subtree)
                results.append(deepcopy(best_subtree))

            # Reset for the next iteration by clearing the "marked" field
            # on the used nodes.
            for node in used_nodes:
                node.unmark()

    return results


def main():
    global simplified_asts_database, exempla_gratis_database
    basicConfig(format="%(levelname)s: %(message)s", level=INFO)

    parser = ArgumentParser(description=__doc__)

    parser.add_argument(
        "simplified_asts_database",
        type=Path,
        help="Path to the simplified ASTs zodb database.",
    )
    parser.add_argument(
        "--exempla-gratis-database",
        type=Path,
        default=DEFAULT_EXEMPLA_GRATIS_DATABASE_DIR,
        help="Output path to the database of exempla gratis examples.",
    )
    parser.add_argument(
        "--num-threads",
        type=int,
        default=DEFAULT_NUM_THREADS,
        help="Number of threads to utilize.",
    )
    parser.add_argument(
        "--max-examples",
        type=int,
        default=DEFAULT_MAX_EXAMPLES,
        help="Maximum number of E.G. examples to generate for each function.",
    )
    parser.add_argument(
        "--seed",
        type=int,
        default=DEFAULT_SEED,
        help="Seed for the random number generator.",
    )
    parser.add_argument(
        "--min-samples",
        type=int,
        default=DEFAULT_MIN_SAMPLES,
        help="Minimum number of trees to use in the E.G. algorithm.",
    )
    parser.add_argument(
        "--max-samples",
        type=int,
        default=DEFAULT_MAX_SAMPLES,
        help="Maximum number of trees to use in the E.G. algorithm.",
    )
    parser.add_argument(
        "--min-iterations",
        type=int,
        default=DEFAULT_MIN_ITERATIONS,
        help="Minimum number of nodes in the example pattern.",
    )
    parser.add_argument(
        "--max-iterations",
        type=int,
        default=DEFAULT_MAX_ITERATIONS,
        help="Maximum number of nodes in the example pattern.",
    )
    parser.add_argument(
        "--specificity-threshold",
        type=float,
        default=DEFAULT_SPECIFICITY_THRESHOLD,
        help="Minimum threshold between the example and total trees.",
    )
    parser.add_argument(
        "--new-edge-threshold",
        type=float,
        default=DEFAULT_NEW_EDGE_THRESHOLD,
        help="Minimum threshold between the best and a new unused edge.",
    )
    parser.add_argument(
        "--max-filler-nodes",
        type=int,
        default=DEFAULT_MAX_FILLER_NODES,
        help="Maximum number of nodes in filler subtrees.",
    )
    parser.add_argument(
        "--max-filler-characters",
        type=int,
        default=DEFAULT_MAX_FILLER_CHARACTERS,
        help="Maximum number of characters in filler subtrees.",
    )

    # Parse the command line.
    args = parser.parse_args()

    # Perform normalization and sanity checks.
    args.simplified_asts_database = args.simplified_asts_database.resolve()
    args.exempla_gratis_database = args.exempla_gratis_database.resolve()
    assert (
        args.simplified_asts_database.exists()
    ), f"{args.simplified_asts_database} does not exist!"
    assert (
        not args.exempla_gratis_database.exists()
    ), f"Output directory {args.exempla_gratis_database} already exists."

    # Setup global variables for the algorithm.
    simplified_asts_database = DatabasePool.pool_create_database(
        args.simplified_asts_database
    )
    exempla_gratis_database = DatabasePool.pool_create_database(
        args.exempla_gratis_database
    )
    config = ExemplaGratisConfig(
        max_examples=args.max_examples,
        seed=args.seed,
        min_samples=args.min_samples,
        max_samples=args.max_samples,
        min_iterations=args.min_iterations,
        max_iterations=args.max_iterations,
        specificity_threshold=args.specificity_threshold,
        new_edge_threshold=args.new_edge_threshold,
        max_filler_nodes=args.max_filler_nodes,
        max_filler_characters=args.max_filler_characters,
    )

    # Iterate over each API in the simplified ASTs database.
    # For each API, attempt to find common patterns of usage
    # using the exempla gratis algorithm.
    def api_examples(api: API_Type) -> Tuple[API_Type, List[SimplifiedAST]]:
        trees = query_trees(simplified_asts_database, *api)
        return api, exempla_gratis(*api, trees, config=config)

    info("Creating common usage examples.")
    apis = list(iter_apis(simplified_asts_database))
    with Pool(args.num_threads) as p:
        results = list(tqdm(p.imap(api_examples, apis), total=len(apis)))

    # Once common examples have been collected, store them in the database.
    info(f"Storing results in {args.exempla_gratis_database}")
    with exempla_gratis_database.transaction() as conn:
        for api, examples in results:
            if examples:
                language, module, function = api
                serialized_examples = [
                    dumps(example, compression="gzip") for example in examples
                ]

                data = conn.root()
                data = get(data, language, PersistentMapping())
                data = get(data, module, PersistentMapping())
                data = get(data, function, PersistentList())
                data.extend(serialized_examples)


if __name__ == "__main__":
    main()
