"""
Utilities and constants common to simplified AST databases.
"""

from typing import Any
from persistent import Persistent
from persistent.mapping import PersistentMapping

HASHES_KEY = "hashes"
TREES_KEY = "trees"
REFS_KEY = "refs"


def get(d: PersistentMapping, key: Any, default: Persistent) -> PersistentMapping:
    """
    Retrieve the value at @d[@key] if possible.  If @key is not
    present, populate @d[@key] with @default.

    :param d: key/value store
    :param key: dictionary key to test for the existance of
    :param default: default value to use if @d[@key] is not present
    """
    d.setdefault(key, default)
    return d.get(key)
