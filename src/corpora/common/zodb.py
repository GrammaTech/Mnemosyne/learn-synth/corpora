"""
Utilities common to ZODB creation scripts.
"""

import argparse
import atexit
import functools
import logging
import pathlib

import tqdm
import ZODB

import kitchensink.utility.filesystem
import kitchensink.utility.zodb

from typing import Callable, Optional


def common_main(
    process: Callable[[pathlib.Path, ZODB.DB], None],
    docstring: Optional[str] = None,
    default_database_dir: Optional[pathlib.Path] = None,
) -> None:
    """
    Functionality common to scripts populating a ZODB instance.

    :param process: function processing source text in a file and populating
        a ZODB instance
    :param docstring: documentation for the command line help
    :param default_database_dir: default path to the zodb instance
    """

    parser = argparse.ArgumentParser(docstring)

    parser.add_argument(
        "corpus",
        type=pathlib.Path,
        help="Path to the ingested corpus/corpora.",
    )
    parser.add_argument(
        "-d",
        "--database",
        type=pathlib.Path,
        default=default_database_dir,
        help="Database directory to create or update.",
    )
    parser.add_argument(
        "-n",
        "--num-files",
        type=int,
        help="Number of files in the corpus to consider.",
    )

    logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)
    args = parser.parse_args()
    args.corpus = args.corpus.resolve()

    logging.info("Initializing ZODB database.")
    database = kitchensink.utility.zodb.create_database(args.database)
    atexit.register(kitchensink.utility.zodb.close_database, database)

    logging.info(f"Collecting python files in {str(args.corpus)}.")
    files = kitchensink.utility.filesystem.python_files(args.corpus)[: args.num_files]

    logging.info("Building ZODB from the collected files.")
    f = functools.partial(process, database=database, corpus=args.corpus)
    list(tqdm.tqdm(map(f, files), total=len(files)))
