"""
Create a ZODB instance with statistical information on function
callsites.
"""

import pathlib

import jedi
import ZODB

import kitchensink.utility.parso as parso
import kitchensink.utility.filesystem as fs

from typing import Dict, List, Optional, Union
from jedi.api.classes import Signature
from persistent.mapping import PersistentMapping
from kitchensink.utility.splitter import PythonRoninSplitter
from corpora.common.zodb import common_main

DEFAULT_DATABASE_DIR = pathlib.Path("callsites.db")


def process(
    path: pathlib.Path,
    database: ZODB.DB,
    *args: List,
    **kwargs: Dict,
) -> None:
    """
    Process the python file at @path, updating the ZODB @database with
    function callarg statistics.

    :param path: python file path
    :param database: open zodb instance with callsite information to populate
    :param lock: database lock for concurrency
    """

    script = jedi.Script(fs.safe_slurp(path))
    tree = script._module_node

    def initialize_nodes(
        root: PersistentMapping, module: str, function: str
    ) -> PersistentMapping:
        """
        Initialize ZODB nodes for the @module/@function if
        they are not present in @root.

        :param root: root node in the ZODB database
        :param module: module name
        :param function: function name
        :return: updated root with root[module][function] initialized
            with an empty persistent dictionary
        """
        if module not in root.keys():
            root[module] = PersistentMapping()

        if function not in root[module].keys():
            root[module][function] = PersistentMapping()

        return root

    def get_param_position(signature: Signature, param_name: str) -> Optional[int]:
        """
        Return the index of @param_name in the function
        @signature, if possible.

        :param param_name: name of a parameter
        :param signature: function signature
        :return: index of @param_name in @signature, if possible
        """
        try:
            params = [param.name for param in signature.params]
            return params.index(param_name)
        except Exception:
            return None

    def update_statistics(
        stats: PersistentMapping,
        arg_keyword_or_pos: Union[str, int],
        arg_type: str,
        item: str,
    ) -> PersistentMapping:
        """
        Update the frequency counter in @stats for the given
        argument keyword or position, argument type (term or
        constant), and item, returning the updated @stats mapping.

        :param stats: mapping of statistical information for a function
        :param arg_keyword_or_pos: argument keyword or position
        :param arg_type: argument type (term or constant)
        :param item: argument value
        :return: @stats after update
        """
        if arg_keyword_or_pos not in stats.keys():
            stats[arg_keyword_or_pos] = PersistentMapping()

        if arg_type not in stats[arg_keyword_or_pos].keys():
            stats[arg_keyword_or_pos][arg_type] = PersistentMapping()

        if item not in stats[arg_keyword_or_pos][arg_type].keys():
            stats[arg_keyword_or_pos][arg_type][item] = 0

        stats[arg_keyword_or_pos][arg_type][item] += 1

        return stats

    # Retrieve all callsites, import statement aliases, and import froms.
    callsites = parso.filter_asts(parso.is_function_callsite, tree)

    # Iterate over callsites, updating the ZODB database with function
    # callarg statistics.
    for callsite in callsites:
        # Retrieve the function signature, module, and name.
        signature = parso.callsite_signature(script, callsite)
        module = parso.callsite_module(script, callsite)
        function = parso.callsite_function(script, callsite)

        if function and module:
            with database.transaction() as conn:
                root = conn.root()

                # Initialize nodes for the current module and
                # function if they do not yet exist.
                initialize_nodes(root, module, function)

                # Retrieve the statistics for the current function.
                stats = root[module][function]

                # Iterate over the callargs and update the ZODB instance.
                callargs = parso.callargs(callsite)
                for position, callarg in enumerate(callargs):
                    keyword = parso.get_keyword(callarg)
                    keyword = keyword.value if keyword else None
                    param_position = (
                        get_param_position(signature, keyword) if signature else None
                    )
                    callarg = parso.normalize_callarg(callarg)

                    # Populate arg_keyword_or_position with the argument
                    # position (either in the function call or function
                    # signature) or the argument keyword passed.
                    if param_position:
                        arg_keyword_or_pos = param_position
                    elif keyword:
                        arg_keyword_or_pos = keyword
                    else:
                        arg_keyword_or_pos = position

                    # Update the statistics, if applicable.
                    if callarg.type == "name":
                        name = callarg.value
                        splitter = PythonRoninSplitter()
                        for term in splitter.split_lower(name):
                            update_statistics(stats, arg_keyword_or_pos, "terms", term)
                    elif parso.is_constant(callarg):
                        constant = callarg.get_code(False).strip()
                        constant = parso.normalize_quotes(constant)
                        update_statistics(
                            stats, arg_keyword_or_pos, "constants", constant
                        )


def main():
    common_main(
        process,
        docstring=__doc__,
        default_database_dir=DEFAULT_DATABASE_DIR,
    )


if __name__ == "__main__":
    main()
