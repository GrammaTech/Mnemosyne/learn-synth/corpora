"""
Script for callsite database post-processing and argument/term scoring.
"""

import argparse
import atexit
import logging
import pathlib

import kitchensink.utility.zodb

from typing import Dict, List, Tuple
from persistent.list import PersistentList
from persistent.mapping import PersistentMapping


DEFAULT_ARG_TERMS_RAW_LIMIT = 50
DEFAULT_ARG_TERMS_PROB_LIMIT = 0.02
DEFAULT_ARG_TERMS_MIN_SAMPLE_SIZE = 5
DEFAULT_CONSTANTS_RAW_LIMIT = 10
DEFAULT_CONSTANTS_PROB_LIMIT = 0.10
DEFAULT_CONSTANTS_MIN_SAMPLE_SIZE = 100


class Config:
    """
    Configuration constants for callsite database post-processing.
    """

    def __init__(
        self,
        arg_terms_raw_limit: int = DEFAULT_ARG_TERMS_RAW_LIMIT,
        arg_terms_prob_limit: float = DEFAULT_ARG_TERMS_PROB_LIMIT,
        arg_terms_min_sample_size: int = DEFAULT_ARG_TERMS_MIN_SAMPLE_SIZE,
        constants_raw_limit: int = DEFAULT_CONSTANTS_RAW_LIMIT,
        constants_prob_limit: float = DEFAULT_CONSTANTS_PROB_LIMIT,
        constants_min_sample_size: int = DEFAULT_CONSTANTS_MIN_SAMPLE_SIZE,
    ):
        """
        :param arg_terms_raw_limit: number of arg terms to keep for predictions
        :param arg_terms_prob_limit: minimum arg term probability to consider
        :param arg_terms_min_sample_size: minimum number of arg term samples
        :param constants_raw_limit: number of constants to keep for predictions
        :param constants_prob_limit: minimim constant probability to consider
        :param constants_min_sample_size: minimum number of constant samples
        """
        self.arg_terms_raw_limit = arg_terms_raw_limit
        self.arg_terms_prob_limit = arg_terms_prob_limit
        self.arg_terms_min_sample_size = arg_terms_min_sample_size
        self.constants_raw_limit = constants_raw_limit
        self.constants_prob_limit = constants_prob_limit
        self.constants_min_sample_size = constants_min_sample_size


def score(
    data: PersistentMapping, config: Config
) -> Dict[str, List[Tuple[str, float]]]:
    """
    Score the given term/constant counts in DATA using the configuration constants
    in CONFIG for filtering.

    :param data: dictionary of term/constant frequency counts
    :param config: configuration constants for database behavior
    :return dictionary of term/constant scores
    """

    def _score(
        items: Dict[str, int],
        count: int,
        raw_limit: int,
        prob_limit: float,
        min_sample_size: int,
    ) -> List[Tuple[str, float]]:
        """
        Score the given dictionary of item -> frequency counts.

        :param data: dictionary mapping items -> frequency counts
        :param count: total to be utilized as a denominator when scoring
        :param raw_limit: maximum number of items to keep after scoring
        :param prob_limit: minimum item score to consider
        :param min_sample_size: minimum number of item samples
        :return list of sorted (item, score) tuples where score is in range (0.0, 1.0]
        """
        results = {}

        # Score the items by divinding the frequency the item appeared
        # by the total.
        for item in items.keys():
            results[item] = items.get(item) / count if count > 0 else 0.0

        # Build the result from the mapping of items -> score.
        # (1) Perform a sanity check that the number of items
        #     meets the minimum sample size.
        # (2) Filter items not achieving the prob_limit.
        # (3) Filter items beyond the raw_limit of terms to return.
        results = results if sum(items.values()) >= min_sample_size else {}
        results = [t for t in results.items() if t[1] > max(prob_limit, 0.0)]
        results = sorted(results, key=lambda t: t[1], reverse=True)
        results = results[:raw_limit]
        return PersistentList(results)

    # Retrive the terms and constants along with their total counts.
    terms = data.get("terms", {})
    constants = data.get("constants", {})
    terms_count = sum(t[1] for t in terms.items())
    constants_count = sum(t[1] for t in constants.items())

    # Score the terms and constants using the counts as a demonimator.
    #
    # Note that for constants, we include the term counts as well,
    # ensuring the probability is based on the total number of items
    # instead of only the total number of constants.  This protects
    # us from returning irrelevant constants in locations where terms
    # are far more likely to appear overall.
    scores = PersistentMapping()
    scores["terms"] = _score(
        terms,
        count=terms_count,
        raw_limit=config.arg_terms_raw_limit,
        prob_limit=config.arg_terms_prob_limit,
        min_sample_size=config.arg_terms_min_sample_size,
    )
    scores["constants"] = _score(
        constants,
        count=terms_count + constants_count,
        raw_limit=config.constants_raw_limit,
        prob_limit=config.constants_prob_limit,
        min_sample_size=config.constants_min_sample_size,
    )

    return scores


def main():
    parser = argparse.ArgumentParser(__doc__)

    parser.add_argument(
        "database", type=pathlib.Path, help="Path to the zodb database."
    )
    parser.add_argument(
        "--arg_terms_raw_limit",
        type=int,
        default=DEFAULT_ARG_TERMS_RAW_LIMIT,
        help="Number of arg terms to keep for predictions.",
    )
    parser.add_argument(
        "--arg_terms_prob_limit",
        type=float,
        default=DEFAULT_ARG_TERMS_PROB_LIMIT,
        help="Minimum arg term probability to consider.",
    )
    parser.add_argument(
        "--arg_terms_min_sample_size",
        type=int,
        default=DEFAULT_ARG_TERMS_MIN_SAMPLE_SIZE,
        help="Minimum number of arg term samples.",
    )
    parser.add_argument(
        "--constants_raw_limit",
        type=int,
        default=DEFAULT_CONSTANTS_RAW_LIMIT,
        help="Number of constants to keep for predictions.",
    )
    parser.add_argument(
        "--constants_prob_limit",
        type=float,
        default=DEFAULT_CONSTANTS_PROB_LIMIT,
        help="Minimum constant probability to consider.",
    )
    parser.add_argument(
        "--constants_min_sample_size",
        type=int,
        default=DEFAULT_CONSTANTS_MIN_SAMPLE_SIZE,
        help="Minimum number of constant samples.",
    )

    logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)
    args = parser.parse_args()
    args.database = args.database.resolve()

    logging.info(f"Loading ZODB database from {str(args.database)}.")
    assert args.database.exists(), f"{str(args.database)} does not exist."
    database = kitchensink.utility.zodb.create_database(args.database)
    atexit.register(kitchensink.utility.zodb.close_database, database)
    config = Config(
        arg_terms_raw_limit=args.arg_terms_raw_limit,
        arg_terms_prob_limit=args.arg_terms_prob_limit,
        arg_terms_min_sample_size=args.arg_terms_min_sample_size,
        constants_raw_limit=args.constants_raw_limit,
        constants_prob_limit=args.constants_prob_limit,
        constants_min_sample_size=args.constants_min_sample_size,
    )

    logging.info("Computing scores for arguments and constants.")
    with database.transaction() as conn:
        root = conn.root()

        for module in root.keys():
            for function in root[module].keys():
                data = root[module][function]
                for position_or_keyword in data.keys():
                    scored = score(data[position_or_keyword], config)
                    data[position_or_keyword] = scored


if __name__ == "__main__":
    main()
