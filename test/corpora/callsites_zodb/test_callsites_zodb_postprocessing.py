"""
Unit tests for callsite database post-processing.
"""

from math import isclose
from sys import maxsize
from corpora.callsites_zodb.callsites_zodb_postprocess import Config, score


# Common test implementations parameterized by argument type (term or constant).
def score_respects_raw_limit_common(argtype: str):
    """
    Test to ensure the [arg_terms|constants]_raw_limit config parameter
    controls the number of results returned by `score`.

    :param argtype: argument type (terms or constants)
    """

    # Create objects utilized by the entire test.
    data = {argtype: {"foo": 1000, "bar": 1000, "baz": 1000}}
    config_kw = "arg_terms_raw_limit" if argtype == "terms" else "constants_raw_limit"

    # Perform tests.
    config = Config(**{config_kw: 0})
    assert len(score(data, config)[argtype]) == 0

    config = Config(**{config_kw: 1})
    assert len(score(data, config)[argtype]) == 1

    config = Config(**{config_kw: 3})
    assert len(score(data, config)[argtype]) == 3

    config = Config(**{config_kw: maxsize})
    assert len(score(data, config)[argtype]) == 3


def score_respects_prob_limit_common(argtype: str):
    """
    Test to ensure the [arg_terms|constants]_prob_limit config parameter
    controls the minimum probability in the results returned by `score`.

    :param argtype: argument type (terms or constants)
    """

    # Create objects utilized by the entire test.
    data = {argtype: {"foo": 100, "bar": 200, "baz": 200}}
    config_kw = "arg_terms_prob_limit" if argtype == "terms" else "constants_prob_limit"

    # Perform tests.
    config = Config(**{config_kw: 1.0})
    assert len(score(data, config)[argtype]) == 0

    config = Config(**{config_kw: 0.2})
    assert len(score(data, config)[argtype]) == 2

    config = Config(**{config_kw: 0.0})
    assert len(score(data, config)[argtype]) == 3

    config = Config(**{config_kw: -maxsize})
    assert len(score(data, config)[argtype]) == 3


def score_respects_sample_size_common(argtype: str) -> None:
    """
    Test to ensure the [arg_terms|constants]_min_sample_size config parameter
    controls the minimum sample size of data required for results to be
    returned from `score`.

    :param argtype: argument type (terms or constants)
    """

    # Create objects utilized by the entire test.
    config_kw = (
        "arg_terms_min_sample_size"
        if argtype == "terms"
        else "constants_min_sample_size"
    )
    config = Config(**{config_kw: 30})

    # Perform tests.
    data = {argtype: {"foo": 0, "bar": 0, "baz": 0}}
    assert len(score(data, config)[argtype]) == 0

    data = {argtype: {"foo": 10, "bar": 10, "baz": 9}}
    assert len(score(data, config)[argtype]) == 0

    data = {argtype: {"foo": 10, "bar": 10, "baz": 10}}
    assert len(score(data, config)[argtype]) == 3


# Unit tests.
def test_score_respects_raw_limit():
    score_respects_raw_limit_common("terms")
    score_respects_raw_limit_common("constants")


def test_score_respects_prob_limit():
    score_respects_prob_limit_common("terms")
    score_respects_prob_limit_common("constants")


def test_score_respects_sample_size():
    score_respects_sample_size_common("terms")
    score_respects_sample_size_common("constants")


def test_scoring():
    """
    Tests to ensure `score` returns appropriate results.
    """
    config = Config(
        constants_raw_limit=maxsize,
        constants_prob_limit=0.01,
        constants_min_sample_size=0,
        arg_terms_raw_limit=maxsize,
        arg_terms_prob_limit=0.01,
        arg_terms_min_sample_size=0,
    )

    # Ensure score does not crash on empty inputs.
    data = {}
    assert score(data, config) == {"constants": [], "terms": []}
    data = {"constants": {}, "terms": {}}
    assert score(data, config) == {"constants": [], "terms": []}

    # Ensure score returns the expected values for a more complex input.
    data = {
        "constants": {"w": 400, "r": 300, "wb": 200, "rb": 100},
        "terms": {"mode": 100, "encoding": 50},
    }
    scored = score(data, config)

    expected_constants = [
        ("w", 400 / 1150),
        ("r", 300 / 1150),
        ("wb", 200 / 1150),
        ("rb", 100 / 1150),
    ]
    assert len(scored["constants"]) == len(expected_constants)
    for expected, actual in zip(scored["constants"], expected_constants):
        assert expected[0] == actual[0]
        assert isclose(expected[1], actual[1])

    expected_terms = [("mode", 100 / 150), ("encoding", 50 / 150)]
    assert len(scored["terms"]) == len(expected_terms)
    for expected, actual in zip(scored["terms"], expected_terms):
        assert expected[0] == actual[0]
        assert isclose(expected[1], actual[1])
