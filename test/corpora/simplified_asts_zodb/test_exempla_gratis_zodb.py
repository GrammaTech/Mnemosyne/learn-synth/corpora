"""
Tests for the exempla_gratis_zodb module.
"""

from pathlib import Path
from pickle import load
from corpora.simplified_asts_zodb.exempla_gratis_zodb import (
    ExemplaGratisConfig,
    exempla_gratis,
)

DATA_DIR = Path(__file__).parent / "data"
DEFAULT_CONFIG = ExemplaGratisConfig(max_examples=3)


def test_exempla_gratis_python_json_dump():
    with open(DATA_DIR / "python-json-dump.pkl", "rb") as f:
        trees = load(f)

    examples = exempla_gratis(trees, "dump", config=DEFAULT_CONFIG)
    actual = [example.snippet() for example in examples]

    expected = [
        "\n".join(
            [
                'with open(${1:path}, "w") as f:',
                "    json.dump(${2:item}, f)",
            ]
        ),
        "\n".join(
            [
                'with pathlib.open("w") as fp:',
                "    json.dump(${1:encrypted, fp})",
            ]
        ),
        "\n".join(
            [
                'with open(${1:path}, "w") as ${2:fp}:',
                "    json.dump(${3:self.current_schema}, ${4:fp}, indent=4)",
            ]
        ),
    ]
    assert actual == expected


def test_exempla_gratis_python_os_makedirs():
    with open(DATA_DIR / "python-os-makedirs.pkl", "rb") as f:
        trees = load(f)

    examples = exempla_gratis(trees, "makedirs", config=DEFAULT_CONFIG)
    actual = [example.snippet() for example in examples]

    expected = [
        "\n".join(
            [
                "os.makedirs(os.path.dirname(${1:filename}), exist_ok=True)",
            ]
        ),
        "\n".join(
            [
                "if not os.path.exists(${1:path}):",
                "    os.makedirs(${2:path})",
            ]
        ),
        "\n".join(
            [
                "os.makedirs(self.${1:ckpt_path}, exist_ok=True)",
            ]
        ),
    ]
    assert actual == expected
